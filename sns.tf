resource "aws_sns_topic" "main"{
  name = var.sns_topic_name
}

resource "aws_sns_topic_policy" "main_policy" {
  arn = aws_sns_topic.main.arn
  policy = var.topic_policy_json != "" ? var.topic_policy_json : <<POLICY
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish",
        "SNS:Receive"
        ],
        "Resource": "${aws_sns_topic.main.arn}",
        "Condition": {
          "ArnLike": {
            "aws:SourceArn": "${aws_s3_bucket.bucket.arn}"
          }
        }
      }
    ]
}
POLICY
}
