{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Resources" : {
    "MySubscription": {
      "Type" : "AWS::SNS::Subscription",
      "Properties" : {
        "Endpoint" : "${email_address}",
        "Protocol" : "${protocol}",
        "TopicArn" : "${topic_arn}"

      }
    }
  }
}
