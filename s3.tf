provider "aws" {}

resource "aws_s3_bucket" "bucket"{
  provider      = aws
  bucket        = var.s3_bucket_name
  acl           = "private"
  force_destroy = var.force_destroy

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.server_side_encryption
        kms_master_key_id = var.kms_master_key_id
      }
    }
  }

  versioning  {
    enabled = var.versioning
  }
}

data "template_file" "cloudformation_sns_stack" {
  template = file("${path.module}/templates/email-sns-stack.json.tpl")

  vars = {
    email_address = var.email_address
    protocol      = var.protocol
    topic_arn     = aws_sns_topic.main.arn
  }
}

resource "aws_cloudformation_stack" "sns-topic" {
  name          = var.stack_name
  template_body = data.template_file.cloudformation_sns_stack.rendered

}
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  topic {
    topic_arn     = aws_sns_topic.main.arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".log"
  }
}
