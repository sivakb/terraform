
variable "region" {
default = "us-west-1"
}

variable "s3_bucket_name" {
  default = "iots3triggeralert"
  description = "bucket name"
}

variable "sns_topic_name" {
  description = "Cloudformation stack name that wraps the SNS topic. Must be unique."
  default     = "s3-iot-sns-topic"
}
variable "force_destroy" {
  default = "false"
  description = "Allow for bucket to get destroyed even files are there"
}

variable "versioning" {
  default = "false"
  description = " Default is false"
}

variable "server_side_encryption" {
  default = "AES256"
  description = " Default is AES256"
}

variable "kms_master_key_id" {
  default = ""
  description = " Default "
}


variable "additional_tags" {
  default     = {}
  description = "The tags to apply to resources created by this module"
  type        = map(string)
}

variable "display_name" {
  type        = string
  description = "Name shown in confirmation emails"
  default     = "tf_sns_email"
}

variable "email_address" {
  type        = string
  description = "Email address to send notifications to"
}


variable "protocol" {
  default     = "email"
  description = "SNS Protocol to use. email or email-json"
  type        = string
}

variable "stack_name" {
  type        = string
  description = "Cloudformation stack name that wraps the SNS topic. Must be unique."
  default     = "stack-name"
}


variable "topic_policy_json" {
  description = "Cloudformation stack name that wraps the SNS topic. Must be unique."
  default     = ""
}
